import React from "react";
import ReactDOM from "react-dom";
import App from "./src/components/app/app";



const rootElement = document.getElementById("root");
ReactDOM.render(
    <App/>,
    rootElement
);
